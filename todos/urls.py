from django.urls import path
from todos.views import viewlist, showsteps, createlist, updatelist

urlpatterns = [
    path("", viewlist, name="todo_list_list"),
    path("<int:id>/", showsteps, name="todo_list_detail"),
    path("create/", createlist, name="todo_list_create"),
    path("<int:id>/edit/", updatelist, name="todo_list_update"),
]
