from django.shortcuts import render, redirect
from .models import TodoList
from .forms import TodoForm


def viewlist(request):
    lists = TodoList.objects.all()
    context = {"list_object": lists}
    return render(request, "todos/list.html", context)


def showsteps(request, id):
    steps = TodoList.objects.get(id=id)
    context = {"showsteps": steps}
    return render(request, "todos/detail.html", context)


def createlist(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list_form = form.save()
            return redirect("todo_list_detail", id=list_form.id)

    else:
        form = TodoForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def updatelist(request, id):
    form_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=form_list)
        if form.is_valid():
            form_list = form.save()
            return redirect("todo_list_detail", id=form_list.id)

    else:
        form = TodoForm(instance=form_list)

    context = {"form": form}

    return render(request, "todos/edit.html", context)
